import axios from 'axios';
import {BASE_URL} from "./config";
//import store from "./store/configurateStore";

const axiosApi = axios.create({
  baseURL: BASE_URL,
});

axiosApi.interceptors.request.use(config => {
/*  const {profile} = store.getState();
  if (profile.data){
    config.headers = {
      Authorization: profile.data.token
    }
  }*/
  return config;
}, error => {
  return Promise.reject({error});
});

export default axiosApi;