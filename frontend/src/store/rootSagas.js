import {all} from 'redux-saga/effects';
import historySagas from "./sagas/historySagas";

export function* rootSagas() {
  yield all([
    ...historySagas,
  ])
}