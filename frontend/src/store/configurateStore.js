import {combineReducers} from "redux";
import {loadFromLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import {rootSagas} from "./rootSagas";

const rootReducer = combineReducers({
  //profile: usersSlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
  preloadedState: persistedState,
});

/*store.subscribe(() => {
  saveToLocalStorage({
    profile: store.getState().profile,
  });
})*/

sagaMiddleware.run(rootSagas);

export default store;