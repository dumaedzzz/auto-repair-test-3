import {createSlice} from "@reduxjs/toolkit";

const name = 'history';

/*export const initialState = {
  data: null,
  errLogIn: null,
  errAccount: null,
  registerLoading: false,
  loginLoading: false,
};*/

const historySlice = createSlice({
  name,
  //initialState,
  reducers: {
    historyPush(state, {payload}) {},
    historyReplace() {},
    historyBack() {},
  }
});

export default historySlice;