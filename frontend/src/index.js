import store from "./store/configurateStore";
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {Router} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import history from "./history";
import App from './App';

import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
    <ToastContainer
      position={"bottom-right"}
    />
    <App/>
    </Router>
  </Provider>
  , document.getElementById('root')
);
