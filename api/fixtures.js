const mongoose = require('mongoose');
const config = require('./config');

const run = async () => {
  await mongoose.connect(config.db.testUrl)

  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const collection of collections) {
    await mongoose.connection.db.dropCollection(collection.name);
  }
/*  const [exm] = await Example.create({

  })*/
  await mongoose.connection.close();

}
run().catch(console.error)
