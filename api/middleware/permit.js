const permit = (...roles) => {
  return (req, res, next) => {
    if (!req.user) return res.status(401).send({error: 'Unauthenticated'});
    if (!roles.includes(req.user.role) || !req.user.activated) return res.status(403).send({error: 'Forbidden'});
    next();
  }
}
module.exports = permit;