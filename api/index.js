const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');

const server = express();
server.use(express.json());
server.use(express.static('public'));
server.use(cors());
const port = 9000;

server.get('/', (req, res) => {
    res.send('Hello team')
})

const run = async () => {
  await mongoose.connect(config.db.testUrl);

  server.listen(port, () => {
    console.log(`Server is started on ${port} port !`);
  })

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
}
run().catch(e => console.error(e));